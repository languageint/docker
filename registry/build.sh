#!/usr/bin/env bash
set -e
DIR=$(dirname $BASH_SOURCE)
DIRABS=$(cd $DIR; pwd)
ROOT=`dirname $DIRABS`

source $DIR/validate-args.sh
source ${ROOT}/registry/.env
[ -f ${ROOT}/registry/.env-local ] && source ${ROOT}/registry/.env-local;
env=$1
service=$2
nc=$BASH_ARGV

build() {
    service_=$1
    if [ "${nocache}" != "--no-cache" ]; then
        nocache=""
    fi
    if [ -d "${service_}" ]; then
        if [ -f "${service_}/Dockerfile.sh" ]; then
            ${service_}/Dockerfile.sh
        fi
        if [ -f "${service_}/Dockerfile" ]; then
            img=${IMAGE_REGISTRY}/${service_}
            version=`date '+%Y.%m.%d'`
            tag="${env}-${version}"
            tagLatest="${env}-latest"
            docker build -t "$img:$tag" ${nocache} ${service_}
            docker tag "$img:$tag" "$img:$tagLatest"
            echo "$img:$tag tag is a current :$tagLatest now"
        fi
    else
        echo "Provided service '${service_}' doesn't exist for environment '${env}'"
        exit 1;
    fi
}

assertEnv $1
if [ "${nc}" == "--no-cache" ]; then
    nocache=${nc}
    if [ "${service}" == "${nc}" ]; then
        service=""
    fi
fi
cd ${ROOT}/${env};
echo "Building $env images...";
if [ -n "${service}" ]; then
    build ${service}
else
    cd ${ROOT}/${env};
    for service_ in *; do
        if [ -d "${service_}" ]; then
            build ${service_}
        fi
    done
fi
