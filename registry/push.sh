#!/usr/bin/env bash
set -e
DIR=$(dirname $BASH_SOURCE)
DIRABS=$(cd $DIR; pwd)
ROOT=`dirname $DIRABS`

source $DIR/validate-args.sh
source ${ROOT}/.env
env=$1
service=$2
nc=$BASH_ARGV

push() {
    service_=$1
    if [ -d "${service_}" ]; then
        if [ -f "${service_}/Dockerfile" ]; then
            img=${IMAGE_REGISTRY}/${service_}
            version=`date '+%Y.%m.%d'`
            tag="${env}-${version}"
            tagLatest="${env}-latest"
            docker push "$img:$tag"
            docker push "$img:$tagLatest"
            docker image rm "$img:$tag"
            echo "$img:$tag tag is the current :$tagLatest now"
        fi
    else
        echo "Provided service '${service_}' doesn't exist for environment '${env}'"
        exit 1;
    fi
}

assertEnv $1
echo "Pushing $env images...";
cd ${ROOT}/${env};
if [ -n "${service}" ]; then
    push ${service}
else
    for service_ in *; do
        echo $service_
        if [ -d "${service_}" ]; then
            push ${service_}
        fi
    done
fi
