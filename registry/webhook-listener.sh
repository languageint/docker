#!/usr/bin/env bash
set -e
cd ${BASH_SOURCE%/*};
source ../.env

RESPONSE="HTTP/1.1 200 OK\r\nConnection: close\r\n\r\n${2:-"OK"}\r\n"

while { echo -en "$RESPONSE"; } | nc -l -q 1 -p "${1:-${REGISTRY_WEBHOOK_PORT}}"; do
  nohup ./process-hook.sh &
  echo "================================================"
done
