DIR=$(dirname $BASH_SOURCE)

BUILDS=(`find $DIR/.. -maxdepth 2 -name ".build"`)
ENVS=()
for b in "${BUILDS[@]}"; do
    ENVS+=(`basename $(dirname $b)`)
done


function isValidEnv {
    for e in "${ENVS[@]}"; do
        if [ "$e" == "$1" ]; then
            return 0
        fi
    done
    return 1
}

function assertEnv {
    if ! isValidEnv $1; then
        echo "NOT VALID ENV: $1 (Valid envs: ${ENVS[@]})"
        exit 1
    fi
}