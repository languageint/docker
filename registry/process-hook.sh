#!/usr/bin/env bash
set -e
DIR=$(dirname $BASH_SOURCE)
DIRABS=$(cd $DIR; pwd)
ROOT=`dirname $DIRABS`
source $DIR/validate-args.sh

message=`git log -1`

notify() {
    msg=$@
    curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"$msg\"}" https://hooks.slack.com/services/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
    echo -e "\033[0;33m$msg\033[0m"
}

function notify-success {
    msg=$1
    msg_="✔️ $msg... *OK*"
    notify $msg_
}

function notify-fail {
    code=$1
    msg=$2
    msg_="💩 $msg... *FAILED($code)*"
    notify $msg_
}

build-notify() {
    env=$1
    set +e
    $DIRABS/build.sh $env
    buildCode=$?
    set -e
    if [ $buildCode -eq 0 ]; then
        set +e
        $DIRABS/push.sh $env
        pushCode=$?
        set -e
        if [ $pushCode -eq 0 ]; then
            notify-success $env
        else
            notify-fail $pushCode "Pushing $env"
        fi
    else
        notify-fail $buildCode "Building $env"
    fi
}

lock=/tmp/process-hock.lock
exec 200>$lock
flock 200
git pull
notify "Last commit\n\`\`\`\n$message\n\`\`\`"
ENVS=(abstract)
for env in "${ENVS[@]}"; do
    build-notify $env
done
