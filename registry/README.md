# Image registry
## Run on staging
It's likely to have the registry running on staging (`test`) environment as it uses AWS S3 as a storage. 
The docker-compose file is placed in the directory `registry` so to make it running, `docker-compose up -d` from within the `registry` directory.

## Building images
Inside `registry` directory, there is the `build.sh` script that requires one parameter that is the environment of the images you want to build for.
Possible environments are: `dev|test|prod|language_test`.
To build all images for desired environment and push them to the registry run `./build.sh (dev|test|prod|language_test) [<service>] [--no-cache]` on staging.
After this, images will be available from the registry under `li-registry:5000` address bounded by the LI VPN area.
The naming convention for images is: `<IMAGE_REGISTRY>/<SERVICE>:<ENV>-<VERSION>`, eg: `li-registry:5000/finance:dev-2018.12.04`. 
To use the latest version, use `<ENV>-latest` tag, eg. `li-registry:5000/finance:dev-latest`

## Enable Webhook for Bitbucket
To enable webhook that builds images after pushing to `docker` repository, run: `sudo systemctl start registry-webhook.service`.
To see the logs, run `tail -f /var/log/syslog | grep registry-webhook` (matching the `registry-webhook` identifier).

## Use registry from dev environment 
To be able to use the images stored in the registry from dev environment, you need to place the registry certificate to your local docker certificates like follows:

```
scp lil@staging:/storage/lil/docker/registry/certs/domain.crt ~/docker-registry.crt
sudo mkdir -p /etc/docker/certs.d/li-registry:5000
sudo mv ~/docker-registry.crt /etc/docker/certs.d/li-registry:5000/ca.crt
```
