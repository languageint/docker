# Docker config
1. Copy the file `etc/docker/daemon.json` into `/etc/docker/daemon.json`
1. Append the content of `etc/subgid` and `etc/subuid` files into your linux config (`/etc/subgid` and `/etc/subuid`)
   and modify if required. Restart the main docker machine service.
1. To use image registry, download and place the certificate:
```
scp lil@staging:/storage/lil/docker/registry/certs/domain.crt ~/docker-registry.crt
sudo mkdir -p /etc/docker/certs.d/li-registry:5000
sudo mv ~/docker-registry.crt /etc/docker/certs.d/li-registry:5000/ca.crt
```

# Haskell services
Once the services are running (after `docker-compose up`),
run `./scripts/hs` to generate the executables and restart the containers,
this action can be used to update the containers after pulling the source code.

Check the man pages for more info

**The containers will not stop properly before this step**


# Deploy web2
web2 = Services that are installed in each slave (LI2)

## Install
Run this once to set up the machine
```
./prod-web2/deploy/install
```
needless to say, the images should be available (read the "make services" section)

## Make services
### PHP 2
*This assumes that the prod environment is installed.*
To generate the container with all the code, run:
```
./scripts/php2-prod && ./scripts/php2-prod-push -y
```

or in the case of updated translations:
```
./scripts/php2-prod -t && ./scripts/php2-prod-push -y
```
this is typically the case after releasing **LI1**

The `-y` is needed to truly publish the changes to the repository,
without it, the changes will be in the local docker (useful for
testing the prod environment in the development machines)

### LI2-HS (Haskell)
*This assumes that the test environment is installed.*
From the **testing** machine (aka *dev* or *staging*), run:
```
./scripts/hs-prod test li2 && ./scripts/hs-prod-push -y li2
```

## Configuration
The default configuration is set for our AWS instances, however
you can deploy this in any enviroment by exporting the proper variables to your system (see the `./prod-web2/.env` file, and `./prod/.env` for building, e.g. `export DB1_IP=172.50.0.1`).
