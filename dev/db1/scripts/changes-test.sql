USE language;

UPDATE `site_culture` SET `domain` = REPLACE(`domain`, 'languageinternational.', 'staging.languageinternational.');
UPDATE `site_culture` SET `domain` = REPLACE(`domain`, 'languageint.', 'staging.languageint.');
UPDATE `site` SET `domain` = REPLACE(`domain`, 'languageinternational.', 'staging.languageinternational.');
UPDATE `site` SET `domain` = REPLACE(`domain`, 'languageint.', 'staging.languageint.');
UPDATE `site` SET `domain` = REPLACE(`domain`, 'openabroad.com', 'staging.openabroad.com');
UPDATE lead_assignment_rule SET visited_domain = REPLACE(visited_domain, 'languageinternational.', 'staging.languageinternational.');
UPDATE lead_assignment_rule SET visited_domain = REPLACE(visited_domain, 'languageint.', 'staging.languageint.');
