CREATE USER 'language'@'%' IDENTIFIED BY 'happy1';
CREATE USER 'lifinance'@'%' IDENTIFIED BY 'happy1';
CREATE USER 'li2hs'@'%' IDENTIFIED BY 'happy1';

GRANT ALL ON *.* TO 'language'@'%';
GRANT ALL ON *.* TO 'lifinance'@'%';
GRANT ALL ON *.* TO 'li2hs'@'%';
FLUSH PRIVILEGES;
