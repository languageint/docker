USE language;

UPDATE site_culture SET domain = REPLACE(domain, 'languageinternational.', 'languageinternational-dev.');
UPDATE site_culture SET domain = REPLACE(domain, 'languageint.', 'languageint-dev.');
UPDATE site SET domain = REPLACE(domain, 'languageinternational.', 'languageinternational-dev.');
UPDATE site SET domain = REPLACE(domain, 'languageint.', 'languageint-dev.');
UPDATE site SET domain = REPLACE(domain, 'openabroad.com', 'openabroad-dev.com');
UPDATE lead_assignment_rule SET visited_domain = REPLACE(visited_domain, 'languageinternational.', 'languageinternational-dev.');
UPDATE lead_assignment_rule SET visited_domain = REPLACE(visited_domain, 'languageint.', 'languageint-dev.');
