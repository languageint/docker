#!/usr/bin/env bash
set -e
PATH_=$(cd "$( dirname "$0" )" && pwd)
SERVICE=$(basename ${PATH_})
ROOT=$(dirname ${PATH_})
env=$(basename ${ROOT})

source ${ROOT}/../.env

cat <<EOF > ${ROOT}/${SERVICE}/Dockerfile
FROM ${IMAGE_REGISTRY}/php2:${env}-latest

SHELL ["/bin/bash", "-c"]

RUN apt-get update \
 && apt-get install -y gnupg2

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
 && apt-get install -y nodejs

RUN npm install -g typescript
RUN npm install -g typings
RUN npm install @types/node --save-dev
RUN npm i -g yarn

WORKDIR /code/apps/updater/out
COPY ./entrypoint.sh /entrypoint.sh
COPY ./command.sh /command.sh
CMD [ "/command.sh" ]
EOF
