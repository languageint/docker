#!/bin/bash
set -e
cd /code
composer install

rm -f /xdebug-profiler/*

exec "$@"
