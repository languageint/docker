#!/bin/bash
set -e
yarn
pm2 start /processes.json

exec "$@"