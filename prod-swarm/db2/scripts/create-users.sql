-- language (web)
DROP USER IF EXISTS 'language'@'%';
CREATE USER 'language'@'%' IDENTIFIED BY 'happy1';
GRANT SELECT, INSERT, UPDATE, DELETE ON language.* TO 'language'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, CREATE TEMPORARY TABLES ON languageint.* TO 'language'@'%';
GRANT ALL ON search.* TO 'language'@'%';
-- languagecli (kind of admin)
DROP USER IF EXISTS 'languagecli'@'%';
CREATE USER 'languagecli'@'%' IDENTIFIED BY 'happy1';
GRANT ALL ON *.* TO 'languagecli'@'%';
FLUSH PRIVILEGES;
