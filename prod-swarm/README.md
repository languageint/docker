# Production environment

By docker swarm.

This is the only environment that must be installed in all of the server
(except tools, that can contain ../prod for tooling).

All the servers should be registered in the `docker-machine` with their proper
names (e.g. `www1`)
