vcl 4.0;

import directors;

backend slave1 {
    .host = "slave1";
    .port = "80";
}

backend slave2 {
    .host = "slave2";
    .port = "80";
}

backend LI2 {
    .host = "nginx";
    .port = "80";
}

backend TOOLS {
    .host = "nginx";
    .port = "80";
}

sub vcl_init {
     new LI1 = directors.round_robin();
     LI1.add_backend(slave1);
     LI1.add_backend(slave2);
}

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "nginx";
    .port = "80";
}

sub vcl_synth {
    if (resp.status == 750) {
        set resp.status = 301;
        set resp.http.Location = regsuball(req.http.host, "^languageint", "www.languageint") + req.url;
        return(deliver);
    }
}

#Call cookie based detection method in vcl_recv.
sub rewrite_cookie {
    if (req.http.cookie ~ "currency=") {
       #unset all the cookie from request except currency
       set req.http.X-Currency = regsub(req.http.cookie, "(.*?)(currency=)([^;]*)(.*)$", "\3");
    }

}

# hosts allowed to purge cache
acl purge {
    "localhost";
    "127.0.0.1";
}

sub vcl_recv {
    # Happens before we check if we have this in cache already.
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.

    # handle purge cache request
    if (req.method == "PURGE") {

        #check if of request
        if (!client.ip ~ purge) {
            return(synth(405, "This IP is not allowed to send PURGE requests."));
        }
        return (purge);
    }

    # passing real ip to backend
    if (req.restarts == 0) {
        if (req.http.X-Forwarded-For) {
           set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + client.ip;
        } else {
           set req.http.X-Forwarded-For = client.ip;
        }
    }

    # add www
#    if (req.http.host ~ "^languageint(ernational)?\..*" ) {
#  	  return (synth (750, ""));
#    }



    if (req.http.host ~ "admin.languageinternational.com"
        || req.http.host ~ "schools.languageinternational.com"
        || req.http.host == "api.li1"
        || req.http.host == "front.li1"
	     ){
      	set req.backend_hint = TOOLS;
        return (pass);
    } elseif (req.http.host ~ "languageint"
    	&& (req.url ~ "^/course/"
        	|| req.url ~ "^/pricing"
        	|| req.url ~ "^/price"
        	|| req.url ~ "^/registration"
        	|| req.url ~ "^/request_info"
        	|| req.url ~ "^/contact"
        	|| req.url ~ "^/change_language"
        	|| req.url ~ "^/thank-you"
        	|| req.url ~ "^/files"
        	|| req.url ~ "^/question"
        	|| req.url ~ "^/ask_question"
        	|| req.url ~ "^/robots"
        	|| req.url ~ "^/css"
        	|| req.url ~ "^/js"
        	|| req.url ~ "^/images"
        	|| req.url ~ "^/favicon.ico"
        	|| req.url ~ "^/payment"
        	|| req.url ~ "^/ccp"
        	|| req.url ~ "^/pag"
        	|| req.url ~ "^/lead"
        	|| req.url ~ "^/rp"
        	|| req.url ~ "^/bbp"
        	|| req.url ~ "^/agent"
        	|| req.url ~ "^/api"
        	|| req.url ~ "^/publisher"
        	|| req.url ~ "^/student"
        	|| req.url ~ "^/promo-code"
        	|| req.url ~ "^/_region_choices"
        	|| req.url ~ "^/home"
        	|| req.url ~ "^/badge"
        	|| req.url ~ "ajax_consolidated_data"
        	|| req.url ~ "write-review"
        	|| req.url ~ "sitemap"
        	|| req.url ~ "&url=")
    	) {
          	set req.backend_hint = LI1.backend();

            if (req.method == "POST" || req.url !~ "^/course/" || req.url ~ "register" ) {
          	    return (pass);
          	}

            # cache static files
          	if (req.url ~ "^[^?]*\.(css|gif|ico|jpeg|jpg|js|ogg|pdf|png|ttf|txt|)(\?.*)?$") {
          	   unset req.http.Cookie;
          	   return (hash);
          	}

        } else {
          	set req.backend_hint = LI2;

            if (req.method == "POST") {
          	    return (pass);
          	}

            # cache static files
          	if (req.url ~ "^[^?]*\.(css|gif|ico|jpeg|jpg|js|ogg|pdf|png|ttf|txt|)(\?.*)?$") {
          	   unset req.http.Cookie;
          	   return (hash);
          	}
        }

        call rewrite_cookie;
        # unset cookie if user did not request currency change
        if (!req.http.Cookie ~ "currency"){
            unset req.http.Cookie;
        }

        return (hash);
        # Happens before we check if we have this in cache already.
        #
        # Typically you clean up the request here, removing cookies you don't need,
        # rewriting the request, etc.
}

sub vcl_hash {
    hash_data(req.url);
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }
    if (req.http.X-Currency) {
        #add cookie in hash
        hash_data(req.http.X-Currency);
    }
    return(lookup);
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.

    # if there is an server error or 404 reduce the time contend is cached
    if (beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504 || beresp.status == 404) {

        set beresp.ttl = 1m;
        set beresp.grace = 1m;
        return (deliver);
    }

    set beresp.ttl = 3h;
    set beresp.grace = 6h;

    if (bereq.url ~ "^[^?]*\.(7z|avi|bz2|flac|flv|gz|mka|mkv|mov|mp3|mp4|mpeg|mpg|ogg|ogm|opus|rar|tar|tgz|tbz|txz|wav|webm|xz|zip)(\?.*)?$") {
      unset beresp.http.set-cookie;
    }
    return (deliver);
}
