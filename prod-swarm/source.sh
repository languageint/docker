export IMAGE_REGISTRY=li-registry:5000
export DB1_IP=10.33.17.16
export LI1_IP=10.33.17.13
export DB2_STORAGE=/storage/mysql/li-db2
export CDN_ADDRESS=https://d2sj6gv6213dvd.cloudfront.net
export CDN_IMAGE_ADDRESS=https://d2sj6gv6213dvd.cloudfront.net
export WWW1_IP=10.33.17.14
export WWW2_IP=10.33.17.15
# Export ports
export REDIS_PORT=6379
export DB2_PORT=3307
export FINANCE_PORT=8081
export LI2HS_PORT=8083
export NGINX_PORT=8080
export RABBITMQ_PORT=5672
export RABBITMQ_UI_PORT=15672
export VARNISH_PORT=80
