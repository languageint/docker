#!/bin/bash
set -e
CWD=`pwd`
cd $CWD/server
yarn
cd $CWD/web
yarn
cd $CWD

exec "$@"
