#!/usr/bin/env bash
set -e

cat /etc/nginx/conf.d/site.conf.template \
  | envsubst '$LI1_PORT,$LI2_PORT' \
  > /etc/nginx/conf.d/default.conf

cat /etc/nginx/conf.d/static.conf.template \
  | envsubst '$STATIC_PORT' \
  > /etc/nginx/conf.d/static.conf

exec "$@"
