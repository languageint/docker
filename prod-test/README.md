This is for testing staging before the deployment.

It is just to mix the LI2 and LI into one port as it is on prod.

The li1 and li2 ports and ips are not part of the configutation,
you should export them on your own, example:
`export LI1_PORT=5667`

Possibly there is a file `~/envs/prod-test` to use as source.

*You should use `docker-compose build` for this enviroment*

