#!/usr/bin/env bash
ROOT=$(dirname $(cd "$(dirname "$0")" ; pwd -P))
source $ROOT/.env

docker network create li --driver=bridge --subnet=${SUBNET}/16
