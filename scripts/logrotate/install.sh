#!/usr/bin/env bash
set -e
dir=$(cd ${BASH_SOURCE%/*}; pwd)
source $dir/../print-colors.sh

while getopts "u:g:n:s:e:" option; do
    case "$option" in
        u) user=$OPTARG;;
        g) group=$OPTARG;;
        n) name=$OPTARG;;
        s) service=$OPTARG;;
        e) env=$OPTARG;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
    esac
done

if [ "$user" == "" ] || [ "$group" == "" ] || [ "$name" == "" ] || [ "$service" == "" ] || [ "$env" == "" ]; then
    error "Missing parameters"
    exit 1
fi;

filepath="/etc/logrotate.d/nginx-$env-$service"
servicepath="$(dirname $(dirname $dir))/prod-$env"
info "Installing logrotate `green "$name"` config in `green $filepath` (`yellow "$user:$group"`)"
cat << EOF > $filepath
/var/log/nginx/$name/*.log {
	daily
	missingok
	rotate 14
	compress
	delaycompress
	notifempty
	create 0640 $user $group
	sharedscripts
	postrotate
		(cd $servicepath && docker-compose exec $service bash -c "kill -USR1 \`cat /var/run/nginx.pid\`")
	endscript
}
EOF
