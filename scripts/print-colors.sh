NC='\033[0m'

function yellow {
    echo -en "\033[0;33m$1$NC"
}
function blue {
    echo -en "\033[0;34m$1$NC"
}
function green {
    echo -en "\033[0;32m$1$NC"
}
function red {
    echo -en "\033[0;31m$1$NC"
}
function bold {
    echo -en "$(tput bold)$1$(tput sgr0)"
}
# Semantic messages
function info {
    echo "ℹ️  $1"
}
function error {
    echo "❌  `red "$1"`"
}
function warning {
    echo "⚠️  `yellow "$1"`"
}
function success {
    echo "✅  `green "$1"`"
}
function begin {
    echo "$1 .. "
}
# for using with ..done (processing in between)
function begin.. {
    echo -n "$1 .. "
}
function ..done {
    echo `green "done"`
}
function finish {
    echo "$1 .. `..done`"
}
