direnv=$(cd ${BASH_SOURCE%/*}; pwd)
source ${direnv}/.env
if [ -f ${direnv}/.env-local ]; then
    source ${direnv}/.env-local;
fi
