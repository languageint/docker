#!/bin/bash
set -e
cd /code/assets
yarn
exec "$@"
