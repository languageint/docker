FROM php:7.2.17-fpm

ENV PHALCON_VERSION=3.4.2

RUN curl -sSL "https://codeload.github.com/phalcon/cphalcon/tar.gz/v${PHALCON_VERSION}" | tar -xz \
 && cd cphalcon-${PHALCON_VERSION}/build \
 && ./install \
 && cp ../tests/_ci/phalcon.ini $(php-config --configure-options | grep -o "with-config-file-scan-dir=\([^ ]*\)" | awk -F'=' '{print $2}') \
 && cd ../../ \
 && rm -r cphalcon-${PHALCON_VERSION}

# Locales
RUN apt-get update \
	&& apt-get install -y locales zlib1g-dev libzip-dev iputils-ping

RUN dpkg-reconfigure --frontend=noninteractive locales \
	&& locale-gen \
	&& /usr/sbin/update-locale LANG=C.UTF-8

RUN echo 'ar_AE.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'bg_BG.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'ca_ES.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'cs_CZ.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'da_DK.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'de_DE.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'el_GR.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'es_ES.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'fi_FI.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'he_IL.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'hu_HU.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'id_ID.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'it_IT.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'ja_JP.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'ko_KR.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'nl_NL.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'pl_PL.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'pt_PT.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'ro_RO.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'ru_RU.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'sk_SK.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'sv_SE.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'th_TH.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'tr_TR.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'uk_UA.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'vi_VN.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'zh_CN.UTF-8 UTF-8' >> /etc/locale.gen \
    && echo 'zh_HK.UTF-8 UTF-8' >> /etc/locale.gen

RUN echo 'LANG="en_US.UTF-8"'>/etc/default/locale \
	&& locale-gen \
	&& dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8

ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Install intl
RUN apt-get install -y libicu-dev \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install intl

# Install redis
RUN pecl install -o -f redis-4.2.0 \
&&  rm -rf /tmp/pear \
&&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini

# enable extensions
RUN docker-php-ext-install opcache
RUN apt-get install -y zlib1g-dev libzip-dev \
    && docker-php-ext-install zip

## Install APC
RUN pecl install apcu
RUN echo "extension=apcu.so" > /usr/local/etc/php/conf.d/24-apcu.ini
RUN pecl install apcu_bc
RUN echo "extension=apc.so" > /usr/local/etc/php/conf.d/25-apc.ini
RUN pecl install ast-0.1.6
RUN echo "extension=ast.so" > /usr/local/etc/php/conf.d/25-ast.ini

RUN docker-php-ext-install calendar pdo_mysql gettext

CMD ["php-fpm", "-F"]
